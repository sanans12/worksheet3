﻿
using System.Runtime.Serialization;

namespace Worksheet3.Models
{
    [DataContract]
    public class GameDto
    {
        public GameDto() { }

        public GameDto(dynamic data)
        {
            Id = data.Id;
            Name = data.Name;
            Price = data.Price;
            Active = data.Active;
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public double Price { get; set; }
        [DataMember]
        public bool Active { get; set; }
    }
}

