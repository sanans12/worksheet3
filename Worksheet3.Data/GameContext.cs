﻿using Microsoft.EntityFrameworkCore;

namespace Worksheet3.Data
{
    public class GameContext : DbContext
    {
        public DbSet<Game> Games { get; set; }

        public GameContext()
        {
        }

        public GameContext(DbContextOptions<GameContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Game>(x =>
            {
                x.Property(y => y.Name).IsRequired();
                x.Property(y => y.Price).IsRequired();
            });
        }
    }
}
