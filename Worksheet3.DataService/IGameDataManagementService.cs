﻿using System.Collections.Generic;
using System.ServiceModel;
using System.Threading.Tasks;
using Worksheet3.Models;

namespace Worksheet3.DataService
{
    [ServiceContract]
    public interface IGameDataManagementService
    {
        [OperationContract]
        Task CreateGame(GameDto gameDto);

        [OperationContract]
        Task<GameDto> GetGame(int id);

        [OperationContract]
        Task<List<GameDto>> GetGames();

        [OperationContract]
        Task UpdateGame(GameDto gameDto);

        [OperationContract]
        Task DeleteBrand(int id);
    }
}