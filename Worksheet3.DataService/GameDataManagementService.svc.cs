﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Worksheet3.Data;
using Worksheet3.Models;

namespace Worksheet3.DataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class GameDataManagementService : IGameDataManagementService
    {
        private readonly DbContextOptions<GameContext> _options;

        public GameDataManagementService()
        {
            _options = new DbContextOptionsBuilder<GameContext>()
                .UseInMemoryDatabase(databaseName: "Worksheet3")
                .Options;

            InitialiseDatabase();
        }

        private async void InitialiseDatabase()
        {
            using (GameContext context = new GameContext(_options))
            {
                await GameDbInitialiser.SeedTestData(context);
            }
        }

        public async Task CreateGame(GameDto gameDto)
        {
            using (GameContext context = new GameContext(_options))
            {
                Game game = new Game
                {
                    Name = gameDto.Name,
                    Price = gameDto.Price,
                    Active = true
                };

                context.Add(game);
                await context.SaveChangesAsync();
            }
        }

        public async Task<GameDto> GetGame(int id)
        {
            using (GameContext context = new GameContext(_options))
            {
                Game game = await context.Games.Where(x => x.Active)
                                               .FirstOrDefaultAsync(x => x.Id == id);

                return new GameDto(game);
            }
        }

        public async Task<List<GameDto>> GetGames()
        {
            using (GameContext context = new GameContext(_options))
            {
                return await context.Games.Where(x => x.Active)
                                           .Select(game => new GameDto(game))
                                           .ToListAsync();
            }
        }

        public async Task UpdateGame(GameDto gameDto)
        {
            using (GameContext context = new GameContext(_options))
            {
                Game game = new Game
                {
                    Id = gameDto.Id,
                    Name = gameDto.Name,
                    Price = gameDto.Price,
                    Active = true
                };

                try
                {
                    context.Update(game);
                    await context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (context.Games.Any(x => x.Id == game.Id))
                    {
                        throw new ArgumentException();
                    }

                    throw;
                }
            }
        }

        public async Task DeleteBrand(int id)
        {
            using (GameContext context = new GameContext(_options))
            {
                Game game = await context.Games.FindAsync(id);

                game.Active = false;

                context.Update(game);
                await context.SaveChangesAsync();
            }
        }
    }
}
