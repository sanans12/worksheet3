﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameDataManagementServiceReference;

namespace Worksheet3.Facade
{
    public interface IGameManagementService
    {
        Task CreateGame(GameDto gameDto);
        Task<GameDto> GetGame(int id);
        Task<List<GameDto>> GetGames();
        Task UpdateGame(GameDto gameDto);
        Task DeleteBrand(int id);
    }
}