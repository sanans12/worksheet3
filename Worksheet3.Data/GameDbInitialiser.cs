﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Worksheet3.Data
{
    public static class GameDbInitialiser
    {
        public static async Task SeedTestData(GameContext context)
        {
            if (context.Games.Any())
            {
                return;
            }

            List<Game> games = new List<Game>
            {
                new Game { Name = "Pokemon Stop!", Price = 0.99, Active = true },
                new Game { Name = "Draw your Friends", Price = 9.99, Active = false },
                new Game { Name = "Mystery Box Tycoon", Price = 30.00, Active = true },
                new Game { Name = "Baking Papa", Price = 49.99, Active = true }
            };

            context.Games.AddRange(games);

            await context.SaveChangesAsync();
        }
    }
}
