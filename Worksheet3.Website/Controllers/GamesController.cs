﻿using System.Threading.Tasks;
using GameDataManagementServiceReference;
using Microsoft.AspNetCore.Mvc;
using Worksheet3.Facade;

namespace Worksheet3.Website.Controllers
{
    public class GamesController : Controller
    {
        private readonly IGameManagementService _gameManagementService;

        public GamesController(IGameManagementService gameManagementService)
        {
            _gameManagementService = gameManagementService;
        }

        // GET: Games
        public async Task<IActionResult> Index()
        {
            return View(await _gameManagementService.GetGames());
        }

        // GET: Games/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameDto gameDto = await _gameManagementService.GetGame(id.Value);

            if (gameDto == null)
            {
                return NotFound();
            }

            return View(gameDto);
        }

        // GET: Games/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Active,Id,Name,Price")] GameDto gameDto)
        {
            if (ModelState.IsValid)
            {
                await _gameManagementService.CreateGame(gameDto);
                return RedirectToAction(nameof(Index));
            }

            return View(gameDto);
        }

        // GET: Games/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameDto gameDto = await _gameManagementService.GetGame(id.Value);

            if (gameDto == null)
            {
                return NotFound();
            }
            return View(gameDto);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Active,Id,Name,Price")] GameDto gameDto)
        {
            if (id != gameDto.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _gameManagementService.UpdateGame(gameDto);

                return RedirectToAction(nameof(Index));
            }
            return View(gameDto);
        }

        // GET: Games/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            GameDto gameDto = await _gameManagementService.GetGame(id.Value);

            if (gameDto == null)
            {
                return NotFound();
            }

            return View(gameDto);
        }

        // POST: Games/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            await _gameManagementService.DeleteBrand(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
