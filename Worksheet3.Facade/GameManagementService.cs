﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameDataManagementServiceReference;

namespace Worksheet3.Facade
{
    public class GameManagementService : IGameManagementService
    {
        private readonly GameDataManagementServiceClient _gameDataManagementServiceClient = new GameDataManagementServiceClient();

        public async Task CreateGame(GameDto gameDto)
        {
            await _gameDataManagementServiceClient.CreateGameAsync(gameDto);
        }

        public async Task<GameDto> GetGame(int id)
        {
            return await _gameDataManagementServiceClient.GetGameAsync(id);
        }

        public async Task<List<GameDto>> GetGames()
        {
            return (await _gameDataManagementServiceClient.GetGamesAsync()).ToList();
        }

        public async Task UpdateGame(GameDto gameDto)
        {
            await _gameDataManagementServiceClient.UpdateGameAsync(gameDto);
        }

        public async Task DeleteBrand(int id)
        {
            await _gameDataManagementServiceClient.DeleteBrandAsync(id);
        }
    }
}